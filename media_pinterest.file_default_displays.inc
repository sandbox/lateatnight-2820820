<?php

/**
 * @file
 * Default display configuration for the default Pinterest file type.
 */

/**
 * Implements hook_file_default_displays().
 */
function media_pinterest_file_default_displays() {
  $file_displays = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'pinterest__default__media_pinterest';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array('maxwidth' => '', 'hidecaption' => FALSE);
  $file_displays['pinterest__default__media_pinterest'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'pinterest__teaser__media_pinterest';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array('maxwidth' => 320, 'hidecaption' => TRUE);
  $file_displays['pinterest__teaser__media_pinterest'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'pinterest__preview__media_pinterest';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array('maxwidth' => 320, 'hidecaption' => TRUE);
  $file_displays['pinterest__preview__media_pinterest'] = $file_display;

  return $file_displays;
}

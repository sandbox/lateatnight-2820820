<?php

/**
 * @file
 * Extends the MediaReadOnlyStreamWrapper class to handle Pinterest objects.
 */

/**
 * Pinterest stream wrapper.
 *
 * Create an instance like this:
 * $pinterest = new MediaPinterestStreamWrapper('pinterest://pin/[code]');.
 */
class MediaPinterestStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'https://www.pinterest.com/pin';

  /**
   * Override MediaReadOnlyStreamWrapper::getMimeType().
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return 'text/pinterest';
  }

  /**
   * Override MediaReadOnlyStreamWrapper::interpolateUrl().
   */
  public function interpolateUrl() {
    if ($params = $this->get_parameters()) {
      return $this->base_url . '/' . $params['pin'];
    }
    return FALSE;
  }
}

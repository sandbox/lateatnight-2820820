<?php

/**
 * @file
 * Template media_pinterest/theme/media-pinterest.tpl.php.
 *
 * Template file for theme('media_pinterestm').
 *
 * Variables available:
 *  $uri - The media uri for the Pinterest (e.g., pinterest://pin/fA9uwTtkSN).
 */
?>
<div class="<?php print $classes; ?>">
  <a href="https://www.pinterest.com/pin/<?php print ($options['pin']); ?>/"
     data-pin-do="embedPin" <?php print($options['hidedescription']?'data-pin-terse="true"':"");?> data-pin-width="<?php print ($options['size']); ?>">
  </a>
</div>
